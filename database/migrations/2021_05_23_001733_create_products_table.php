<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('slug')->unique();
            $table->text('detail');
            $table->text('description');
            $table->string('image')->nullable();
            $table->boolean('status')->default(1);
            $table->boolean('hot')->default(0);
            $table->boolean('recommended')->default(0);
            $table->double('price', 8, 2);
            $table->double('amount', 8, 2);
            $table->text('link');
            $table->text('tags');
            $table->timestamps();
            $table->foreignId('category_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
