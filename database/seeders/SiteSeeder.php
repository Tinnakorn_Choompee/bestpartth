<?php

namespace Database\Seeders;

use App\Models\Site;
use Illuminate\Database\Seeder;

class SiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Site::create(['name' => 'หน้าหลัก',     'title' => 'หน้าหลัก | BESTPARTTH', 'description' => 'หน้าหลัก | BESTPARTTH', 'keywords' => 'สินค้าออนไลน์, แนะนำสินค้า']);
        Site::create(['name' => 'ประเภทสินค้า', 'title' => 'ประเภทสินค้า | BESTPARTTH', 'description' => 'ประเภทสินค้า | BESTPARTTH', 'keywords' => 'สินค้าออนไลน์, แนะนำสินค้า']);
        Site::create(['name' => 'แท็กสินค้า',    'title' => 'แท็กสินค้า | BESTPARTTH', 'description' => 'หน้าหลัก | BESTPARTTH', 'keywords' => 'สินค้าออนไลน์, แนะนำสินค้า']);
    }
}
