<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        $user = User::create([
            'name' => 'admin',
            'username' => 'admin',
            'password' => Hash::make('password'),
        ]);

        $user->attachRole('admin');

        $user = User::create([
            'name' => 'user',
            'username' => 'user',
            'password' => Hash::make('password'),
        ]);

        $user->attachRole('user');
    }
}
