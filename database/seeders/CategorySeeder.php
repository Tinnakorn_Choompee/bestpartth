<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Category::factory()->count(3)->create();
        Category::create(['name' => 'เสื้อผ้าแฟชั่นผู้ชาย', 'status' => 1]);
        Category::create(['name' => 'เครื่องใช้ในบ้าน', 'status' => 1]);
        Category::create(['name' => 'สื่อบันเทิงภายในบ้าน', 'status' => 1]);
        Category::create(['name' => 'มือถือและอุปกรณ์เสริม', 'status' => 1]);

        Category::create(['name' => 'เสื้อยืด', 'status' => 1, 'parent_id' => 1]);
        Category::create(['name' => 'ชุดชั้นในชาย', 'status' => 1, 'parent_id' => 1]);
        Category::create(['name' => 'กางเกงขายาว', 'status' => 1, 'parent_id' => 1]);

        Category::create(['name' => 'อุปกรณ์ปรับปรุงบ้าน', 'status' => 1, 'parent_id' => 2]);
        Category::create(['name' => 'ห้องครัว', 'status' => 1, 'parent_id' => 2]);
        Category::create(['name' => 'ห้องนอน', 'status' => 1, 'parent_id' => 2]);

        Category::create(['name' => 'หูฟัง', 'status' => 1, 'parent_id' => 3]);
        Category::create(['name' => 'อุปกรณ์ทีวี', 'status' => 1, 'parent_id' => 3]);
        Category::create(['name' => 'เครื่องเสียง', 'status' => 1, 'parent_id' => 3]);

        Category::create(['name' => 'โทรศัพท์มือถือ', 'status' => 1, 'parent_id' => 4]);
        Category::create(['name' => 'Apple', 'status' => 1, 'parent_id' => 14]);
    }
}
