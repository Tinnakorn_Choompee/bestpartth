<?php

namespace Database\Seeders;

use App\Models\Site;
use App\Models\User;
use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class TruncateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Site::truncate();
        Category::truncate();
        User::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
