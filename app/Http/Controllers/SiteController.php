<?php

namespace App\Http\Controllers;

use App\Models\Site;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function edit($site)
    {
        $site = Site::where('name', $site)->first();
        return view('backend.site.edit', ['site' => $site]);
    }

    public function update(Request $request, Site $site)
    {
        $site->title = $request->title;
        $site->description = $request->description;
        $site->keywords = $request->keywords;
        $site->save();
        return redirect()->route('dashboard')->with('success', trans('alert.updated'));
    }
}
