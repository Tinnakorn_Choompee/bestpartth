<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\StoreRequest;
use App\Http\Requests\Category\UpdateRequest;
use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $categories = $request->get('search') ? Category::latest()->name($request->get('search'))->paginate(10) : Category::latest()->paginate(10);
        return view('backend.categories.index', ['categories' => $categories]);
    }

    public function create()
    {
        return view('backend.categories.create', ['parents' => Category::all()]);
    }

    public function store(StoreRequest $request)
    {
        Category::create($request->all());
        return redirect()->route('categories.index')->with('success', trans('alert.created'));
    }

    public function show(Category $category)
    {
        return view('backend.categories.show', ['category' => $category]);
    }

    public function edit(Category $category)
    {
        return view('backend.categories.edit', ['category' => $category])->with('parents', Category::latest()->get());
    }

    public function update(UpdateRequest $request, Category $category)
    {
        Category::where('id', $category->id)->update($request->only(['name', 'status', 'parent_id']));
        return redirect()->route('categories.index')->with('success', trans('alert.updated'));
    }

    public function destroy(Category $category)
    {
        $category->delete();
        return back()->with('success', trans('alert.deleted'));
    }
}
