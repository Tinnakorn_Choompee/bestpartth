<?php

namespace App\Http\Controllers;

use App\Http\Requests\Tag\StoreRequest;
use App\Http\Requests\Tag\UpdateRequest;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function index(Request $request)
    {
        $tags = $request->get('search') ? Tag::latest()->name($request->get('search'))->paginate(10) : Tag::latest()->paginate(10);
        return view('backend.tags.index', ['tags' => $tags]);
    }

    public function create()
    {
        return view('backend.tags.create');
    }

    public function store(StoreRequest $request)
    {
        Tag::create($request->all());
        return redirect()->route('tags.index')->with('success', trans('alert.created'));
    }

    public function edit(Tag $tag)
    {
        return view('backend.tags.edit', ['tag' => $tag]);
    }

    public function update(UpdateRequest $request, Tag $tag)
    {
        Tag::where('id', $tag->id)->update($request->only(['name']));
        return redirect()->route('tags.index')->with('success', trans('alert.updated'));
    }

    public function destroy(Tag $tag)
    {
        $tag->delete();
        return back()->with('success', trans('alert.deleted'));
    }
}
