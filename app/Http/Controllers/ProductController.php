<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\Product;
use App\Models\Category;
use App\Models\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Http\Requests\Product\StoreRequest;
use App\Http\Requests\Product\UpdateRequest;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $products = $request->get('search') ? Product::latest()->name($request->get('search'))->paginate(10) : Product::latest()->paginate(10);
        return view('backend.products.index', ['products' => $products]);
    }

    public function create()
    {
        return view('backend.products.create')->with('categories', Category::active()->get())->with('tags', Tag::all());
    }

    public function store(StoreRequest $request)
    {
        $imageName = pathinfo($request->image->getClientOriginalName(), PATHINFO_FILENAME).time().'.'.$request->image->extension();
        $request->image->move('images/product', $imageName);

        $product = new Product();
        $product->category_id = $request->category_id;
        $product->name        = $request->name;
        $product->slug        = str_replace([':', '\\', '/', '*', ' '], '-', $request->name);
        $product->detail      = $request->detail;
        $product->price       = $request->price;
        $product->amount      = $request->amount;
        $product->status      = $request->status;
        $product->hot         = $request->hot;
        $product->recommended = $request->recommended;
        $product->description = $request->description;
        $product->link        = $request->link;
        $product->tags        = $request->tags;
        $product->image       = $imageName;
        $product->save();

        if ($request->hasFile('gallery')) {
            $images = [];
            if($files = $request->file('gallery')){
                foreach($files as $file){
                    $name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME).time().'.'.$file->extension();
                    $file->move('images/gallery',$name);
                    $images[]=$name;
                }
            }

            foreach($images as $image) {
                $gallery = new Gallery();
                $gallery->image = $image;
                $gallery->product_id = $product->id;
                $gallery->save();
            }
        }

        return redirect()->route('products.index')->with('success', trans('alert.created'));
    }

    public function show(Product $product)
    {
        return view('backend.products.show', ['product' => $product]);
    }

    public function edit(Product $product)
    {
        $select_tags = [];
        foreach(Tag::all() as $key => $tag) {
            if (in_array($tag->name, explode(",", $product->tags))) {
                array_push($select_tags, $key);
            }
        }
        return view('backend.products.edit', ['product' => $product])->with('categories', Category::where('status', 1)->get())->with('tags', Tag::all())->with('select_tags', $select_tags);
    }

    public function update(UpdateRequest $request, Product $product)
    {
        if ($request->hasFile('image')) {
            File::delete('images/product/'.$product->image);
            $imageName = pathinfo($request->image->getClientOriginalName(), PATHINFO_FILENAME).time().'.'.$request->image->extension();
            $request->image->move('images/product', $imageName);
        }

        $product->category_id = $request->category_id;
        $product->name        = $request->name;
        $product->slug        = str_replace([':', '\\', '/', '*', ' '], '-', $request->name);
        $product->detail      = $request->detail;
        $product->price       = $request->price;
        $product->amount      = $request->amount;
        $product->status      = $request->status;
        $product->hot         = $request->hot;
        $product->recommended = $request->recommended;
        $product->description = $request->description;
        $product->link        = $request->link;
        $product->tags        = $request->tags;
        $product->image       = $imageName ?? $product->image;
        $product->save();

        if ($request->hasFile('gallery')) {
            foreach($product->gallery as $gallery) {
                File::delete('images/gallery/'.$gallery->image);
            }
            $product->gallery()->each(function($gallery) {
                $gallery->delete();
            });

            $images = [];
            if($files = $request->file('gallery')){
                foreach($files as $file){
                    $name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME).time().'.'.$file->extension();
                    $file->move('images/gallery',$name);
                    $images[]=$name;
                }
            }

            foreach($images as $image) {
                $gallery = new Gallery();
                $gallery->image = $image;
                $gallery->product_id = $product->id;
                $gallery->save();
            }
        }

        return redirect()->route('products.index')->with('success', trans('alert.updated'));
    }

    public function destroy(Product $product)
    {
        foreach($product->gallery as $gallery) {
            File::delete('images/gallery/'.$gallery->image);
        }
        File::delete('images/product/'.$product->image);
        $product->delete();
        return back()->with('success', trans('alert.deleted'));
    }
}
