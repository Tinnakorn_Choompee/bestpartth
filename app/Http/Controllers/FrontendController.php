<?php

namespace App\Http\Controllers;

use App\Models\Site;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index(Request $request)
    {
        $products = $request->get('search') ? Product::latest()->active()->name($request->get('search'))->paginate(12) : Product::latest()->paginate(12);
        return view('frontend.index', ['products' => $products])
            ->with(['site' => Site::where('name', 'หน้าหลัก')->first()])
            ->with(['Recommended' => Product::recommended()]);
    }

    protected function set_category($item)
    {
        if ($item->child->count()) {
            foreach($item->child as $c) {
                session()->push('category_id', $c->id);
                $this->set_category($c);
            }
        }
    }

    public function category(Request $request, $slug)
    {
        session()->forget('category_id');
        $category = Category::where('name', $slug)->first();
        $category_id = [];
        $category_set = [];

        session('category_id', []);

        if ($category->child->count()) {
            foreach ($category->child as $item) {
                array_push($category_id, $item->id);
                $this->set_category($item);
            }
        }

        if($category->child->count() == 0 || $category->parent_id == 0) {
            array_push($category_id, $category->id);
        }

        $category_set = !session('category_id') ? $category_id : array_merge($category_id, session('category_id'));

        $products = $request->get('search') ? 
            Product::latest()->active()->whereIn('category_id', $category_set)->name($request->get('search'))->paginate(12) : 
            Product::latest()->active()->whereIn('category_id', $category_set)->paginate(12);;

        return view('frontend.category', ['products' => $products])
            ->with(['site' => Site::where('name', 'ประเภทสินค้า')->first()])
            ->with('category_name', $slug)
            ->with(['Recommended' => Product::recommended()]);
    }

    public function tag(Request $request, $slug)
    {
        $products = $request->get('search') ?
            Product::latest()->active()->name($request->get('search'))->tag($slug)->paginate(12) :
            Product::latest()->active()->tag($slug)->paginate(12);

        return view('frontend.tag', ['products' => $products])
            ->with(['site' => Site::where('name', 'แท็กสินค้า')->first()])
            ->with('tag_name', $slug)
            ->with(['Recommended' => Product::recommended()]);
    }

    public function product($slug)
    {
        $product = Product::detail($slug);
        return view('frontend.detail', ['product' => $product])
            ->with(['related' => Product::where('category_id', $product->category_id)->where('id', '!=', $product->id)->get()]);
    }
}
