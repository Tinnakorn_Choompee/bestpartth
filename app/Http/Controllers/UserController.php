<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;

class UserController extends Controller
{
    public function index()
    {
        return view('backend.users.index', ['users' =>  User::latest()->paginate(10)]);
    }

    public function create()
    {
        return view('backend.users.create')->with('roles', Role::all());
    }

    public function store(StoreRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'password' => Hash::make($request->password),
        ]);

        $user->attachRoles($request->roles);

        return redirect()->route('users.index')->with('success', trans('alert.created'));
    }

    public function show(User $user)
    {
        return view('backend.users.show', ['user' => $user]);
    }

    public function edit(User $user)
    {
        return view('backend.users.edit', ['user' => $user])->with('roles', Role::all());
    }

    public function update(UpdateRequest $request, User $user)
    {
        User::where('id', $user->id)->update([
            'name' => $request->name,
            'username' => $request->username,
            'password' => Hash::make($request->password),
        ]);

        $user->syncRoles($request->roles);

        return redirect()->route('users.index')->with('success', 'Updated!');
    }

    public function destroy(User $user)
    {
       $user->delete();
       return back()->with('success', trans('alert.deleted'));
    }
}
