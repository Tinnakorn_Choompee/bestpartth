<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'roles' => 'required|array',
            'name' => 'required|string|max:255|unique:users,name,'.$this->user->id,
            'username' => 'required|string|max:255|unique:users,username,'.$this->user->id,
            'password' => 'required|string|confirmed|min:8',
        ];
    }

    public function messages()
    {
        return [
            'roles.required' => trans('validate.roles.required'),
            'name.unique'  => trans('validate.name.unique'),
            'username.unique' => trans('validate.username.required'),
            'name.required'  => trans('validate.name.required'),
            'username.required' => trans('validate.username.required'),
            'password.required' => trans('validate.password.required'),
            'password.min' => trans('validate.password.min', ['min' => 8]),
            'password.confirmed' => trans('validate.password.confirmed'),
        ];
    }
}
