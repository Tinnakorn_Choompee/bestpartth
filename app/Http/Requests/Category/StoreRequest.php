<?php

namespace App\Http\Requests\Category;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255|unique:categories',
            'status' => 'required',
            'parent_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.unique' => trans('validate.name.unique'),
            'name.required' => trans('validate.name.required'),
            'parent_id.required' => trans('validate.parent_id.required')
        ];
    }
}
