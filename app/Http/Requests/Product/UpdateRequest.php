<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255|unique:products,name,'.$this->product->id,
            'detail' => 'required',
            'price' => 'required|integer',
            'amount' => 'required|integer',
            'description' => 'required',
            'tags' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => trans('validate.name.required'),
            'name.unique' => trans('validate.name.unique'),
            'detail.required' => trans('validate.detail.required'),
            'price.required' => trans('validate.price.required'),
            'amount.required' => trans('validate.amount.required'),
            'description.required' => trans('validate.description.required'),
            'tags.required' => trans('validate.tags.required'),
        ];
    }
}
