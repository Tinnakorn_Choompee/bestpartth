<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255|unique:products',
            'detail' => 'required',
            'price' => 'required|integer',
            'amount' => 'required|integer',
            'category_id' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg',
            'description' => 'required',
            'tags' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => trans('validate.name.required'),
            'name.unique' => trans('validate.name.unique'),
            'detail.required' => trans('validate.detail.required'),
            'price.required' => trans('validate.price.required'),
            'amount.required' => trans('validate.amount.required'),
            'category_id.required' => trans('validate.category.required'),
            'image.required' => trans('validate.image.required'),
            'image.mimes' => trans('validate.image.mimes'),
            'description.required' => trans('validate.description.required'),
            'tags.required' => trans('validate.tags.required'),
        ];
    }
}
