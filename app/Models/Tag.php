<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function scopeName($query, $search)
    {
        return $query->where('name', 'like', '%'.$search.'%');
    }
}
