<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function gallery()
    {
        return $this->hasMany(Gallery::class);
    }

    public function scopeRecommended($query)
    {
        return $query->where('recommended', 1)->select('name', 'slug', 'amount', 'image')->get();
    }

    public function scopeDetail($query, $slug)
    {
        return $query->where('slug', $slug)->first();
    }

    public function scopeName($query, $search)
    {
        return $query->where('name', 'like', '%'.$search.'%');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeTag($query, $tag)
    {
        return $query->where('tags', 'like', '%'.$tag.'%');
    }
}
