<?php

use App\Models\Tag;
use App\Models\Site;
use App\Models\User;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TagController;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\FrontendController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontendController::class, 'index'])->name('index');

Route::get('/product/{product}', [FrontendController::class, 'product'])->name('product.detail');

Route::get('/category/{category}', [FrontendController::class, 'category'])->name('product.category');

Route::get('/tag/{tag}', [FrontendController::class, 'tag'])->name('product.tag');

Route::middleware(['auth'])->group(function () {
    Route::view('dashboard', 'dashboard', [
        'user' => User::count(),
        'category' => Category::count(),
        'tag' => Tag::count(),
        'product' => Product::count(),
        'sites' => Site::all(),
        'latest_product' => Product::take(5)->latest()->get()
    ])->name('dashboard');
    Route::middleware(['role:admin'])->group(function () {
        Route::get('site/{site}', [SiteController::class, 'edit'])->name('site.edit');
        Route::patch('site/{site}', [SiteController::class, 'update'])->name('site.update');

        Route::resource('users', UserController::class);
        Route::resource('categories', CategoryController::class);
        Route::resource('tags', TagController::class);
    });
    Route::middleware(['role:admin|user'])->group(function () {
        Route::resource('products', ProductController::class);
    });
});

Route::get('locale/{locale}', function ($locale) {
     session()->put('locale', $locale);
     return redirect()->back();
})->name('locale');

require __DIR__.'/auth.php';
