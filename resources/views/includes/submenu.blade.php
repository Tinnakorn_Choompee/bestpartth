@if ($item->child->count())
<ul>
    @foreach ($item->child as $c)
        <li><a href="{{ route('product.category', $c->name) }}"> {{ $c->name }}</a>
            @includeWhen($c->child->count(), 'includes.submenu', ['item' => $c, 'count' => $c->child->count() + $count])
        </li>
    @endforeach
</ul>
@endif