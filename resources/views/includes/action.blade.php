<span class="lg:hidden absolute top-0 left-0 bg-blue-200 px-2 py-1 text-xs font-bold uppercase">{{ __('table.actions') }}</span>
<div class="flex item-center justify-center">
    @if(in_array($name, ['users', 'categories', 'products']))
    <a href="{{ route($name.'.show', $id) }}">
        <div class="w-5 mr-2 transform text-blue-500 hover:text-blue-500 hover:scale-110">
            <svg fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z" />
            </svg>
        </div>
    </a>
    @endif
    @permission($name.'-update')
    <a href="{{ route($name.'.edit', $id) }}">
        <div class="w-5 mr-2 transform text-yellow-500 hover:text-yellow-500 hover:scale-110">
            <svg fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z"  />
            </svg>
        </div>
    </a>
    @endpermission
    @permission($name.'-delete')
    <form method="POST" action="{{ route($name.'.destroy', $id) }}" onsubmit="event.preventDefault(); confirm({{ $id }});" id="delete_{{ $id }}">
        @csrf
        @method('DELETE')
        <button type="submit" class="w-5 mr-2 transform text-red-500 hover:text-red-500 hover:scale-110">
            <svg fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
            </svg>
        </button>
    </form>
    @endpermission
</div>

<script>
    function confirm(id) {
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                title: 'sweet-title',
                confirmButton: 'focus:outline-none text-white text-sm py-2.5 px-5 rounded-md bg-red-500 hover:bg-red-600 hover:shadow-lg flex items-center mr-3',
                cancelButton: 'focus:outline-none text-white text-sm py-2.5 px-5 rounded-md bg-gray-500 hover:bg-gray-600 hover:shadow-lg flex items-center'
            },
            buttonsStyling: false
        });
        swalWithBootstrapButtons.fire({
            title: "{{ __('form.confirm_delete') }}",
            showCancelButton: true,
            confirmButtonText: "{{ __('form.delete') }}",
            cancelButtonText: "{{ __('form.cancel') }}",
        }).then((result) => {
            if (result.isConfirmed) {
                document.getElementById("delete_"+ id).submit();
                return true
            } else {
                return false
            }
        })
    }
</script>