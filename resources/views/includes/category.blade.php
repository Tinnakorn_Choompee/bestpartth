@if ($item->child->count())
    @foreach ($item->child as $c)
    <option value="{{ $c->id }}" {{ $category_parent == $c->id ? 'selected' : '' }} {{ $c->id == $category_id ? 'disabled' : '' }}>
        @for ($i = 0; $i < $count; $i++)
            &nbsp;&nbsp;
        @endfor
        ↪ {{ $c->name }}
    </option>
    @includeWhen($c->child->count(), 'includes.category', ['item' => $c, 'count' => $c->child->count() + $count])
    @endforeach
@endif