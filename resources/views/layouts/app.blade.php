<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <link rel="icon" type="image/ico" href="{{ asset('logo.ico') }}"/>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>

        <!-- Packages -->
        <script src="{{ asset('vendor/sweetalert/sweetalert2@11.js') }}"></script>
        <script src="https://cdn.tiny.cloud/1/9dnq643n9k5slv417cx7c7ax2wxieqywjrrng5kcp1agq7xm/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    </head>
    <body class="font-sans antialiased">
        <div class="min-h-screen bg-gray-100">
            @include('layouts.navigation')

            <!-- Page Heading -->
            <header class="bg-white shadow">
                <div class="max-w-7xl mx-auto w-screen flex flex-row items-center py-2 px-2 justify-between bg-white shadow-xs">
                      @isset($create)
                      <div class="ml-8 my-4 text-lg text-gray-700">{{ $header }}</div>
                      <div class="flex-row-reverse mr-8">
                        <div class="inline-block mr-2 m-2">
                            <a href="{{ $create }}" class="inline-flex items-center h-10 px-4 focus:outline-none text-green-600 text-sm py-2.5 rounded-full border border-green-600 hover:bg-green-50">
                                <svg xmlns="http://www.w3.org/2000/svg" class="inline-block h-5 w-5 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                                </svg>
                                {{ __('messages.add') }}
                            </a>
                        </div>
                      </div>
                      @endisset
                      @empty($create)
                        @if (!request()->routeIs('dashboard'))
                            <div class="flex-row-reverse mr-8">
                                <div class="inline-block ml-8 m-2">
                                    <a href="{{ $back }}" class="inline-flex items-center h-10 px-4 focus:outline-none text-gray-600 text-sm py-2.5 rounded-full border border-gray-600 hover:bg-gray-50">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                            <path fill-rule="evenodd" d="M9.707 14.707a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 1.414L7.414 9H15a1 1 0 110 2H7.414l2.293 2.293a1 1 0 010 1.414z" clip-rule="evenodd" />
                                        </svg>
                                        {{ __('messages.back') }}
                                    </a>
                                </div>
                            </div>
                            <div class="mr-8 my-4 text-lg text-gray-700">{{ $header }}</div>
                        @else
                            <div class="ml-8 my-4 text-lg text-gray-700">{{ $header }}</div>
                        @endif
                      @endempty
                  </div>
            </header>

            <!-- Page Content -->
            <main>
                {{ $slot }}
            </main>
        </div>
    </body>
</html>
