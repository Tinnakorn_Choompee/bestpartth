<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('messages.show', ['name' => trans('messages.category')]) }}
        </h2>
    </x-slot>

    <x-slot name="back"> {{ route('categories.index') }} </x-slot>
    
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="container px-5 py-5 mx-auto flex flex-col">
                        <div class="lg:w-full mx-auto">
                          <div class="flex flex-col sm:flex-row mt-10">
                            <div class="sm:w-1/3 text-center sm:pr-8 sm:py-8">
                              <div class="w-20 h-20 rounded-full inline-flex items-center justify-center bg-gray-200 text-gray-400">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                                  </svg>
                              </div>
                              <div class="flex flex-col items-center text-center justify-center">
                                <h2 class="font-medium title-font mt-4 mb-6 text-gray-900 text-lg"> {{ $category->name }}</h2>
                                <div class="w-12 h-1 bg-blue-500 rounded mt-2 mb-4"></div>
                                <h2 class="text-gray-900 font-medium title-font tracking-wider text-sm m-2"> {{ __('table.parent') }}</h2>
                                <p class="text-gray-500 mt-2">{{ $category->parent->name ?? null }}</p>
                              </div>
                            </div>
                            <div class="sm:w-2/3 sm:pl-8 sm:py-8 sm:border-l border-gray-200 sm:border-t-0 border-t mt-4 mb-6 pt-4 sm:mt-0 text-center sm:text-left">
                                <div class="container px-5 py-5 mx-auto">
                                    <div class="flex flex-wrap">
                                        @if ($category->child->count())
                                            @foreach ($category->child as $item)
                                            <div class="p-2 lg:w-1/2 md:w-1/2 w-full">
                                                <div class="h-full flex items-center border-gray-200 border p-4 rounded-lg">
                                                <div class="flex-grow">
                                                    <h2 class="text-gray-900 title-font font-medium mb-2"> {{ __('table.child') }} : {{ $item->name }} </h2>
                                                </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        @else
                                        <p class="text-gray-500 mt-2">{{ __('table.no_child') }}</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                          </div>
                        </div>
                      </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
