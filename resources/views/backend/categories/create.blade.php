<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('messages.create', ['name' => trans('messages.category')]) }}
        </h2>
    </x-slot>

    <x-slot name="back"> {{ route('categories.index') }} </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    <form method="POST" action="{{ route('categories.store') }}">
                        @csrf
                        <div class="mt-4 mb-6">
                            <x-label for="name" :value="__('form.name')" />
                            <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus />
                        </div>
                        <div class="mt-4 mb-6">
                            <x-label for="name" :value="__('form.status')" />
                            <x-select name="status" class="block mt-1 w-full">
                                <option value="1" selected> {{ __('form.status_detail.active') }} </option>
                                <option value="0"> {{ __('form.status_detail.in_active') }} </option>
                            </x-select>
                        </div>
                        <div class="mt-4 mb-6">
                            <x-label for="parent" :value="__('form.parent')" />
                            <x-select name="parent_id" id="parent_id" class="block mt-1 w-full">
                                <option value="" selected disabled> {{ __('form.select_parent') }} </option>
                                <option value="0">  {{ __('form.no_parent') }} </option>
                                @foreach ($parents as $p)
                                    @if ($p->child->count() && $p->parent_id == 0)
                                        <option value="{{ $p->id }}"> {{ $p->name }} </option>
                                        @foreach ($p->child as $item)
                                            <option value="{{ $item->id }}">  ↪  {{ $item->name }} </option>
                                            @includeWhen($item->child->count(), 'includes.category', ['item' => $item, 'count' => $item->child->count(), 'category_parent' => null, 'category_id' => null])
                                        @endforeach
                                    @endif
                                    @if($p->parent_id == 0 && $p->child->count() == 0)
                                        <option value="{{ $p->id }}"> {{ $p->name }} </option>
                                    @endif
                                @endforeach
                            </x-select>
                        </div>
                        <div class="flex items-center justify-center mt-4 mb-6">
                            <x-button class="w-full my-5 h-12 text-base">
                                {{ __('form.save') }}
                            </x-button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>


