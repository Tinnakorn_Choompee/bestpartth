<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('messages.show', ['name' => trans('messages.product')]) }}
        </h2>
    </x-slot>

    <x-slot name="back"> {{ route('products.index') }} </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="container px-5 py-5 mx-auto flex flex-col">
                        <div class="lg:w-full mx-auto">
                            <div class="flex flex-col sm:flex-row mt-10">
                                <div class="sm:w-1/3 text-center sm:pr-8 sm:py-8">
                                    @if ($product->image)
                                    <div class="inline-flex items-center justify-center">
                                        <img src="{{ asset('images/product/'. $product->image )}}" alt="">
                                    </div>
                                    @else
                                    <div class="w-20 h-20 rounded-full inline-flex items-center justify-center bg-gray-200 text-gray-400">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                                            viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                                        </svg>
                                    </div>
                                    @endif
                                    <div class="flex flex-col items-center text-center justify-center">
                                        <h2 class="font-medium title-font mt-4 mb-6 text-gray-900 text-lg">
                                            {{ $product->name }}</h2>
                                        <div class="w-12 h-1 bg-blue-500 rounded mt-2 mb-4"></div>
                                        <h2 class="text-gray-900 font-medium title-font tracking-wider text-sm m-2">
                                            {{ __('product.detail') }}</h2>
                                        <p class="text-gray-500 mt-2">{!! $product->detail ?? null !!}</p>
                                        <div class="w-12 h-1 bg-blue-500 rounded mt-6 mb-4"></div>
                                        <h2
                                            class="text-gray-900 font-medium title-font tracking-wider text-sm m-2 mt-4">
                                            {{ __('product.price') }}</h2>
                                        <p class="text-gray-500 mt-2">{!! number_format($product->price) ?? null !!}</p>
                                        <h2 class="text-gray-900 font-medium title-font tracking-wider text-sm m-2">
                                            {{ __('product.amount') }}
                                        </h2>
                                        <p class="text-gray-500 mt-2">{!! number_format($product->amount) ?? null !!} </p>
                                        <h2 class="text-gray-900 font-medium title-font tracking-wider text-sm m-2">
                                            {{ __('product.status') }}
                                        </h2>
                                        <p class="text-gray-500 mt-2">
                                            @switch($product->status)
                                                @case(0) <span class="bg-red-200 text-gray-600 py-1 px-3 rounded-full text-xs">{{ __('form.status_detail.in_active') }}</span>  @break
                                                @case(1) <span class="bg-green-200 text-gray-600 py-1 px-3 rounded-full text-xs">{{ __('form.status_detail.active') }}</span> @break
                                            @default
                                            @endswitch
                                        </p>
                                    </div>
                                </div>
                                <div
                                    class="sm:w-2/3 sm:pl-8 sm:py-8 sm:border-l border-gray-200 sm:border-t-0 border-t mt-4 mb-6 pt-4 sm:mt-0 text-center sm:text-left">
                                    <div class="container mx-auto">
                                        <div class="flex flex-wrap">
                                            <div class="p-1">
                                                @if ($product->hot)
                                                    <hr>
                                                    <h2 class="tracking-widest text-sm title-font font-medium text-gray-500 m-5">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" style="display:inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 18.657A8 8 0 016.343 7.343S7 9 9 10c0-2 .5-5 2.986-7C14 5 16.09 5.777 17.656 7.343A7.975 7.975 0 0120 13a7.975 7.975 0 01-2.343 5.657z" />
                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9.879 16.121A3 3 0 1012.015 11L11 14H9c0 .768.293 1.536.879 2.121z" />
                                                        </svg>
                                                        {{ __('product.hot') }}
                                                    </h2>
                                                    <hr>
                                                @endif
                                                @if ($product->recommended)
                                                <hr>
                                                    <h2 class="tracking-widest text-sm title-font font-medium text-gray-500 m-5">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" style="display:inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z" />
                                                    </svg>
                                                    {{ __('product.recommended') }}
                                                    </h2>
                                                <hr>
                                                @endif
                                                <h2 class="tracking-widest text-sm title-font font-medium text-gray-500 mb-1 mt-5">
                                                    {{ __('product.category') }}
                                                </h2>
                                                <h1 class="title-font text-lg font-medium text-black mb-3">
                                                    {{ $product->category->name }}
                                                </h1>
                                                <h2 class="tracking-widest text-sm title-font font-medium text-gray-500 mb-1 mt-5">
                                                    {{ __('product.description') }}
                                                </h2>
                                                <p class="leading-relaxed mb-3">
                                                    {!! $product->description !!}
                                                </p>
                                                <h2 class="tracking-widest text-sm title-font font-medium text-gray-500 mb-1 mt-5">
                                                    {{ __('product.tags') }}
                                                </h2>
                                                <p class="leading-relaxed mb-3">
                                                    @foreach (explode(",", $product->tags) as $tag)
                                                    <span class="inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-blue-100 bg-blue-700 rounded">
                                                        {{ $tag }}
                                                    </span>
                                                    @endforeach
                                                </p>
                                                <h2 class="tracking-widest text-sm title-font font-medium text-gray-500 mb-1 mt-5">
                                                    {{ __('product.link') }}
                                                </h2>
                                                <p class="leading-relaxed mb-3">
                                                    {!! $product->link !!}
                                                </p>
                                            </div>
                                            <hr>
                                            <section class="py-5 px-1 mt-5">
                                                <div class="grid grid-cols-4">
                                                    @forelse ($product->gallery as $gallery)
                                                        <li class="block p-1 w-2/2 md:w-4/4 h-48">
                                                            <img class="img-preview w-full h-full sticky object-cover rounded-md bg-fixed" src="{{ asset('/images/gallery/'.$gallery->image)}}" alt="no data" />
                                                        </li>
                                                    @empty
                                                        <p> No Images </p>
                                                    @endforelse
                                                  </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>