<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('messages.edit', ['name' => trans('messages.product')]) }}
        </h2>
    </x-slot>

    <x-slot name="back"> {{ route('products.index') }} </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    <form method="POST" action="{{ route('products.update', $product->id) }}" enctype="multipart/form-data">
                        @method('PATCH')
                        @csrf
                        <div class="mt-4 mb-6">
                            <x-label for="name" :value="__('form.name')" />
                            <x-input id="name" class="block mt-1 w-full" type="text" name="name" value="{{ $product->name }}" required autofocus />
                        </div>
                        <div class="mt-4 mb-6">
                            <x-label for="detail" :value="__('form.detail')" />
                            <textarea name="detail" id="detail">{{ $product->detail }}</textarea>
                        </div>
                        <div class="mt-4 mb-6">
                            <x-label for="price" :value="__('form.price')" />
                            <x-input id="price" class="block mt-1 w-full" type="number" name="price" value="{{ $product->price }}" required placeholder="0.00" />
                        </div>
                        <div class="mt-4 mb-6">
                            <x-label for="amount" :value="__('form.amount')" />
                            <x-input id="amount" class="block mt-1 w-full" type="number" name="amount" value="{{ $product->amount }}" required placeholder="0.00" />
                        </div>
                        <div class="mt-4 mb-6">
                            <x-label for="status" :value="__('form.status')" />
                            <x-select name="status" class="block mt-1 w-full">
                                <option value="1" {{ $product->status == 1 ? 'selected' : '' }}> {{ __('form.status_detail.active') }} </option>
                                <option value="0" {{ $product->status == 0 ? 'selected' : '' }}> {{ __('form.status_detail.in_active') }} </option>
                            </x-select>
                        </div>
                        <div class="mt-4 mb-6">
                            <x-label for="hot" :value="__('form.hot')" />
                            <x-select name="hot" class="block mt-1 w-full">
                                <option value="1" {{ $product->hot == 1 ? 'selected' : '' }}> {{ __('form.status_detail.active') }} </option>
                                <option value="0" {{ $product->hot == 0 ? 'selected' : '' }}> {{ __('form.status_detail.in_active') }} </option>
                            </x-select>
                        </div>
                        <div class="mt-4 mb-6">
                            <x-label for="recommended" :value="__('form.recommended')" />
                            <x-select name="recommended" class="block mt-1 w-full">
                                <option value="1" {{ $product->recommended == 1 ? 'selected' : '' }}> {{ __('form.status_detail.active') }} </option>
                                <option value="0" {{ $product->recommended == 0 ? 'selected' : '' }}> {{ __('form.status_detail.in_active') }} </option>
                            </x-select>
                        </div>
                        <div class="mt-4 mb-6">
                            <x-label for="category_id" :value="__('form.category')" />
                            <x-select name="category_id" id="category_id" class="block mt-1 w-full">
                                @foreach ($categories as $p)
                                    @if ($p->child->count() && $p->parent_id == 0)
                                        <option value="{{ $p->id }}" {{ $product->category_id == $p->id ? 'selected' : '' }}> {{ $p->name }} </option>
                                        @foreach ($p->child as $item)
                                            <option value="{{ $item->id }}" {{ $product->category_id == $item->id ? 'selected' : '' }}>  ↪ {{ $item->name }} </option>
                                            @includeWhen($item->child->count(), 'includes.category', ['item' => $item, 'count' => $item->child->count(), 'category_parent' => $product->category_id, 'category_id' => null])
                                        @endforeach
                                    @endif
                                    @if($p->parent_id == 0 && $p->child->count() == 0)
                                        <option value="{{ $p->id }}"> {{ $p->name }} </option>
                                    @endif
                                @endforeach
                            </x-select>
                        </div>
                        <div class="mt-4 mb-6">
                            <x-label for="image" :value="__('form.image')" />
                            <label
                                class="w-100 flex flex-col items-center px-4 py-6 bg-white text-blue rounded-lg tracking-wide uppercase border border-blue cursor-pointer hover:bg-blue hover:text-gray">
                                <svg class="w-8 h-8" fill="currentColor" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 20 20">
                                    <path
                                        d="M16.88 9.1A4 4 0 0 1 16 17H5a5 5 0 0 1-1-9.9V7a3 3 0 0 1 4.52-2.59A4.98 4.98 0 0 1 17 8c0 .38-.04.74-.12 1.1zM11 11h3l-4-4-4 4h3v3h2v-3z" />
                                </svg>
                                <span class="mt-2 text-base leading-normal"> {{ __('form.select_file') }} </span>
                                <input type='file' class="hidden" name="image" id="main_image" />
                            </label>
                            <section class="hero container max-w-screen-lg mx-auto py-6">
                                <img class="mx-auto w-60 rounded-md" src="{{ asset('/images/product/'. $product->image )}}" alt="screenshot" id="preview">
                            </section>
                        </div>
                        <div class="mt-4 mb-6">
                            <x-label for="gallery" :value="__('form.gallery')" />
                            <article aria-label="File Upload Modal" class="relative h-full flex flex-col bg-white rounded-md" ondrop="dropHandler(event);" ondragover="dragOverHandler(event);" ondragleave="dragLeaveHandler(event);" ondragenter="dragEnterHandler(event);">
                                <div id="overlay" class="w-full h-full absolute top-0 left-0 pointer-events-none z-50 flex flex-col items-center justify-center rounded-md">
                                  <i>
                                    <svg class="fill-current w-12 h-12 mb-3 text-blue-700" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                      <path d="M19.479 10.092c-.212-3.951-3.473-7.092-7.479-7.092-4.005 0-7.267 3.141-7.479 7.092-2.57.463-4.521 2.706-4.521 5.408 0 3.037 2.463 5.5 5.5 5.5h13c3.037 0 5.5-2.463 5.5-5.5 0-2.702-1.951-4.945-4.521-5.408zm-7.479-1.092l4 4h-3v4h-2v-4h-3l4-4z" />
                                    </svg>
                                  </i>
                                  <p class="text-lg text-blue-700"> Drop files to upload </p>
                                </div>
                                <section class="h-full overflow-auto p-1 w-full flex flex-col">
                                  <header class="border-dashed border-2 border-gray-400 py-12 flex flex-col justify-center items-center">
                                    <input id="hidden_input" type="file" multiple class="hidden" name="gallery[]"/>
                                    <button type="button" id="button" class="mt-2 rounded-sm px-3 py-1 bg-gray-200 hover:bg-gray-300 focus:shadow-outline focus:outline-none">
                                        {{ __('form.upload_file') }}
                                    </button>
                                  </header>
                                  <h1 class="pt-8 pb-3 font-semibold sm:text-lg text-gray-900">
                                    {{ __('form.upload_list') }}
                                  </h1>
                                    <ul id="gallery" class="flex flex-1 flex-wrap -m-1">
                                        <li id="empty" class="h-full w-full text-center flex flex-col items-center justify-center">
                                            <img class="mx-auto w-32" src="{{ asset('/img/no_data.png')}}" alt="no data" />
                                            <span class="text-small text-gray-500">{{ __('form.no_files_selected') }}</span>
                                        </li>
                                    </ul>
                                    <br>
                                    <div class="grid grid-cols-4 mt-8" id="gallery_show">
                                        @foreach ($product->gallery as $gallery)
                                            <li class="block p-1 w-2/2 md:w-4/4 h-48">
                                                <img class="w-full h-full sticky object-cover rounded-md bg-fixed" src="{{ asset('/images/gallery/'.$gallery->image)}}" alt="no data" />
                                            </li>
                                        @endforeach
                                    </div>
                                </section>
                            </article>

                            <template id="image-template">
                                <li class="block p-1 w-2/2 sm:w-1/3 md:w-1/4 lg:w-1/6 xl:w-1/8 h-44">
                                    <article tabindex="0"
                                        class="group hasImage w-full h-full rounded-md focus:outline-none focus:shadow-outline bg-gray-100 cursor-pointer relative text-transparent hover:text-white shadow-sm">
                                        <img alt="upload preview" class="img-preview w-full h-full sticky object-cover rounded-md bg-fixed" />
                                        <section
                                            class="flex flex-col rounded-md text-xs break-words w-full h-full z-20 absolute top-0 py-2 px-3">
                                            <h1 class="flex-1"></h1>
                                            <div class="flex">
                                                <span class="p-1">
                                                    <i>
                                                        <svg class="fill-current w-4 h-4 ml-auto pt-"
                                                            xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                            viewBox="0 0 24 24">
                                                            <path
                                                                d="M5 8.5c0-.828.672-1.5 1.5-1.5s1.5.672 1.5 1.5c0 .829-.672 1.5-1.5 1.5s-1.5-.671-1.5-1.5zm9 .5l-2.519 4-2.481-1.96-4 5.96h14l-5-8zm8-4v14h-20v-14h20zm2-2h-24v18h24v-18z" />
                                                        </svg>
                                                    </i>
                                                </span>
                                                <p class="p-1 size text-xs"></p>
                                            </div>
                                        </section>
                                    </article>
                                </li>
                            </template>
                        </div>
                        <div class="mt-4 mb-6">
                            <x-label for="description" :value="__('form.description')" />
                            <textarea name="description" id="description">{{ $product->description }}</textarea>
                        </div>
                        <div class="mt-4 mb-6">
                            <x-label for="link" :value="__('form.link')" />
                            <x-input id="link" class="block mt-1 w-full" type="text" name="link" value="{{ $product->link }}" required />
                        </div>
                        <div class="mt-4 mb-6">
                            <x-label for="tag" :value="__('form.tags')" />
                            <div class="overflow-y-scroll h-48">
                                <select x-cloak id="select" name="tags" required>
                                    @foreach ($tags as $tag)
                                        <option value="{{ $tag->name }}"> {{ $tag->name }}</option>
                                    @endforeach
                                </select>
                                <div x-data="dropdown()" x-init="loadOptions()" class="w-full items-center mx-auto">
                                    <input name="tags" type="hidden" x-bind:value="selectedValues()">
                                    <div class="items-center relative">
                                        <div x-on:click="open" class="w-full">
                                            <div class="my-2 p-1 flex border border-gray-200 bg-white rounded">
                                                <div class="flex flex-auto flex-wrap">
                                                    <template x-for="(option,index) in selected"
                                                        :key="options[option].value">
                                                        <div
                                                            class="flex justify-center items-center m-1 font-medium py-1 px-1 bg-white rounded border">
                                                            <div class="text-xs font-normal leading-none max-w-full flex-initial x-model="
                                                                options[option] x-text="options[option].text"></div>
                                                            <div class="flex flex-auto flex-row-reverse">
                                                                <div x-on:click.stop="remove(index,option)">
                                                                    <svg class="fill-current h-4 w-4 " role="button"
                                                                        viewBox="0 0 20 20">
                                                                        <path d="M14.348,14.849c-0.469,0.469-1.229,0.469-1.697,0L10,11.819l-2.651,3.029c-0.469,0.469-1.229,0.469-1.697,0
                                                                         c-0.469-0.469-0.469-1.229,0-1.697l2.758-3.15L5.651,6.849c-0.469-0.469-0.469-1.228,0-1.697s1.228-0.469,1.697,0L10,8.183
                                                                         l2.651-3.031c0.469-0.469,1.228-0.469,1.697,0s0.469,1.229,0,1.697l-2.758,3.152l2.758,3.15
                                                                         C14.817,13.62,14.817,14.38,14.348,14.849z" />
                                                                    </svg>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </template>
                                                    <div x-show="selected.length == 0" class="flex-1">
                                                        <input placeholder="{{ __('form.select_tags') }}" class="bg-transparent p-1 px-2 appearance-none outline-none h-full w-full text-gray-800" x-bind:value="selectedValues()">
                                                    </div>
                                                </div>
                                                <div
                                                    class="text-gray-300 w-8 py-1 pl-2 pr-1 border-l flex items-center border-gray-200 svelte-1l8159u">
                                                    <button type="button" x-show="isOpen() === true" x-on:click="open"
                                                        class="cursor-pointer w-6 h-6 text-gray-600 outline-none focus:outline-none">
                                                        <svg version="1.1" class="fill-current h-4 w-4" viewBox="0 0 20 20">
                                                            <path
                                                                d="M17.418,6.109c0.272-0.268,0.709-0.268,0.979,0s0.271,0.701,0,0.969l-7.908,7.83 c-0.27,0.268-0.707,0.268-0.979,0l-7.908-7.83c-0.27-0.268-0.27-0.701,0-0.969c0.271-0.268,0.709-0.268,0.979,0L10,13.25 L17.418,6.109z" />
                                                        </svg>
                                                    </button>
                                                    <button type="button" x-show="isOpen() === false" @click="close"
                                                        class="cursor-pointer w-6 h-6 text-gray-600 outline-none focus:outline-none">
                                                        <svg class="fill-current h-4 w-4" viewBox="0 0 20 20">
                                                            <path
                                                                d="M2.582,13.891c-0.272,0.268-0.709,0.268-0.979,0s-0.271-0.701,0-0.969l7.908-7.83 c0.27-0.268,0.707-0.268,0.979,0l7.908,7.83c0.27,0.268,0.27,0.701,0,0.969c-0.271,0.268-0.709,0.268-0.978,0L10,6.75L2.582,13.891z " />
                                                        </svg>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="w-full px-4">
                                            <div x-show.transition.origin.top="isOpen()"
                                                class="absolute shadow top-100 bg-white z-40 w-full left-0 rounded max-h-select"
                                                x-on:click.away="close">
                                                <div class="flex flex-col w-full overflow-y-auto">
                                                    <template x-for="(option,index) in options" :key="option"
                                                        class="overflow-auto">
                                                        <div class="cursor-pointer w-full border-gray-100 rounded-t border-b hover:bg-gray-100"
                                                            @click="select(index,$event)">
                                                            <div
                                                                class="flex w-full items-center p-2 pl-2 border-transparent border-l-2 relative">
                                                                <div class="w-full items-center flex justify-between">
                                                                    <div class="mx-2 leading-6" x-model="option"
                                                                        x-text="option.text"></div>
                                                                    <div x-show="option.selected">
                                                                        <svg class="svg-icon" viewBox="0 0 20 20">
                                                                            <path fill="none"
                                                                                d="M7.197,16.963H7.195c-0.204,0-0.399-0.083-0.544-0.227l-6.039-6.082c-0.3-0.302-0.297-0.788,0.003-1.087 C0.919,9.266,1.404,9.269,1.702,9.57l5.495,5.536L18.221,4.083c0.301-0.301,0.787-0.301,1.087,0c0.301,0.3,0.301,0.787,0,1.087 L7.741,16.738C7.596,16.882,7.401,16.963,7.197,16.963z" />
                                                                        </svg>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </template>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flex items-center justify-center mt-4 mb-6">
                            <x-button class="w-full my-5 h-12 text-base">
                                {{ __('form.save') }}
                            </x-button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<script>
    function dropdown() {
        return {
                options: [],
                selected: @json($select_tags),
                show: false,
            open() { this.show = true },
            close() { this.show = false },
            isOpen() { return this.show === true },
            select(index, event) {
                if (!this.options[index].selected) {
                    this.options[index].selected = true;
                    this.options[index].element = event.target;
                    this.selected.push(index);
                } else {
                    this.selected.splice(this.selected.lastIndexOf(index), 1);
                    this.options[index].selected = false
                }
            },
            remove(index, option) {
                this.options[option].selected = false;
                this.selected.splice(index, 1);
            },
            loadOptions() {
                const options = document.getElementById('select').options;
                for (let i = 0; i < options.length; i++) {
                    this.options.push({
                        value: options[i].value,
                        text: options[i].innerText,
                        selected: options[i].getAttribute('selected') != null ? options[i].getAttribute('selected') : false
                    });
                }
            },
            selectedValues(){
                return this.selected.map((option)=>{
                    return this.options[option].value;
                })
            }
        }
    }
</script>

<script>
    main_image.onchange = evt => {
        const [file] = main_image.files
        if (file) {
            preview.src = URL.createObjectURL(file)
        }
    }
</script>

<script>
    const fileTempl = document.getElementById("file-template"),
      imageTempl = document.getElementById("image-template"),
      empty = document.getElementById("empty");
      gallery_show = document.getElementById("gallery_show");
    let FILES = {};
    function addFile(target, file) {
      const isImage = file.type.match("image.*"),
        objectURL = URL.createObjectURL(file);
      const clone = isImage
        ? imageTempl.content.cloneNode(true)
        : fileTempl.content.cloneNode(true);
      clone.querySelector("h1").textContent = file.name;
      clone.querySelector("li").id = objectURL;
      clone.querySelector(".size").textContent =
        file.size > 1024
          ? file.size > 1048576
            ? Math.round(file.size / 1048576) + "mb"
            : Math.round(file.size / 1024) + "kb"
          : file.size + "b";
      isImage &&
        Object.assign(clone.querySelector("img"), {
          src: objectURL,
          alt: file.name
        });
      empty.classList.add("hidden");
      gallery_show.classList.add("hidden");
      target.prepend(clone);
      FILES[objectURL] = file;
    }
    const gallery = document.getElementById("gallery"),
      overlay = document.getElementById("overlay");
    const hidden = document.getElementById("hidden_input");
    document.getElementById("button").onclick = () => hidden.click();
    hidden.onchange = (e) => {
          while (gallery.children.length > 0) {
            gallery.lastChild.remove();
        }
        FILES = {};
        empty.classList.remove("hidden");
        gallery.append(empty);
      for (const file of e.target.files) {
        addFile(gallery, file);
      }
    };
    const hasFiles = ({ dataTransfer: { types = [] } }) =>
      types.indexOf("Files") > -1;
    let counter = 0;
    function dropHandler(ev) {
      ev.preventDefault();
      for (const file of ev.dataTransfer.files) {
        addFile(gallery, file);
        overlay.classList.remove("draggedover");
        counter = 0;
      }
    }
    function dragEnterHandler(e) {
      e.preventDefault();
      if (!hasFiles(e)) {
        return;
      }
      ++counter && overlay.classList.add("draggedover");
    }

    function dragLeaveHandler(e) {
      1 > --counter && overlay.classList.remove("draggedover");
    }

    function dragOverHandler(e) {
      if (hasFiles(e)) {
        e.preventDefault();
      }
    }
</script>

<style>
    [x-cloak] {
        display: none;
    }

    .svg-icon {
        width: 1em;
        height: 1em;
    }

    .svg-icon path,
    .svg-icon polygon,
    .svg-icon rect {
        fill: #333;
    }

    .svg-icon circle {
        stroke: #4691f6;
        stroke-width: 1;
    }

    .hasImage:hover section {
        background-color: rgba(5, 5, 5, 0.4);
    }

    .hasImage:hover button:hover {
        background: rgba(5, 5, 5, 0.45);
    }

    #overlay p,
    i {
        opacity: 0;
    }

    #overlay.draggedover {
        background-color: rgba(255, 255, 255, 0.7);
    }

    #overlay.draggedover p,
    #overlay.draggedover i {
        opacity: 1;
    }

    .group:hover .group-hover\:text-blue-800 {
        color: #2b6cb0;
    }
</style>

<script>
    const plugins = [
        'advlist autolink link image lists charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
        'table emoticons template paste'
    ];
    const toolbar = 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons';
    const menu = {
        file: { title: 'File', items: 'newdocument restoredraft | preview | print ' },
        edit: { title: 'Edit', items: 'undo redo | cut copy paste | selectall | searchreplace' },
        view: { title: 'View', items: 'code | visualaid visualchars visualblocks | spellchecker | preview fullscreen' },
        insert: { title: 'Insert', items: 'image link media template codesample inserttable | charmap emoticons hr | pagebreak nonbreaking anchor toc | insertdatetime' },
        format: { title: 'Format', items: 'bold italic underline strikethrough superscript subscript codeformat | formats blockformats fontformats fontsizes align lineheight | forecolor backcolor | removeformat' },
        tools: { title: 'Tools', items: 'spellchecker spellcheckerlanguage | code wordcount' },
        table: { title: 'Table', items: 'inserttable | cell row column | tableprops deletetable' }
    }
    tinymce.init({
        branding: false,
        selector: 'textarea#detail',
        height : 350,
        plugins: plugins,
        toolbar: toolbar,
        menu: menu
    });
    tinymce.init({
        branding: false,
        selector: 'textarea#description',
        height : 550,
        plugins: plugins,
        toolbar: toolbar,
        menu: menu
    });
</script>