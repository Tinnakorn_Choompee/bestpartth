<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('menu.user') }}
        </h2>
    </x-slot>

    @permission('users-create')
    <x-slot name="create"> {{ route('users.create') }} </x-slot>
    @endpermission

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <table class="border-collapse w-full">
                        <thead>
                            <tr>
                                <th class="p-3 font-bold uppercase bg-gray-200 text-gray-600 border border-gray-300 hidden lg:table-cell"> # </th>
                                <th class="p-3 font-bold uppercase bg-gray-200 text-gray-600 border border-gray-300 hidden lg:table-cell"> {{ __('table.name') }} </th>
                                <th class="p-3 font-bold uppercase bg-gray-200 text-gray-600 border border-gray-300 hidden lg:table-cell"> {{ __('table.username') }} </th>
                                <th class="p-3 font-bold uppercase bg-gray-200 text-gray-600 border border-gray-300 hidden lg:table-cell"> {{ __('table.actions') }} </th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($users as $item)
                            <tr class="bg-white lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0">
                                <td class="w-full lg:w-1/12 p-3 text-gray-800 text-center border border-b block lg:table-cell relative lg:static">
                                    <span class="lg:hidden absolute top-0 left-0 bg-blue-200 px-2 py-1 text-xs font-bold uppercase"> # </span>
                                    {{ ++$loop->index }}
                                </td>
                                <td class="w-full lg:w-1/2 p-3 text-gray-800 text-left xs:text-center border border-b block lg:table-cell relative lg:static">
                                    <span class="lg:hidden absolute top-0 left-0 bg-blue-200 px-2 py-1 text-xs font-bold uppercase">{{ __('table.name') }}</span>
                                    {{ $item->name }}
                                </td>
                                <td class="w-full lg:w-1/4 p-3 text-gray-800 text-center border border-b block lg:table-cell relative lg:static">
                                    <span class="lg:hidden absolute top-0 left-0 bg-blue-200 px-2 py-1 text-xs font-bold uppercase">{{ __('table.username') }}</span>
                                    {{ $item->username }}
                                </td>
                                <td class="w-full lg:w-2/12 p-3 text-gray-800 text-center border border-b block lg:table-cell relative lg:static">
                                    @include('includes.action', ['name' => 'users', 'id' => $item->id])
                                </td>
                            </tr>
                            @empty
                            <tr class="bg-white lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0">
                                <td colspan="4" class="w-full lg:w-auto p-3 text-gray-800 text-center border border-b block lg:table-cell relative lg:static">
                                    {{ __('table.no_data') }}...
                                </td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                    <div class="items-center mt-5">
                        <div class="row m-auto">
                            {{ $users->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

@if (session('success'))
<script>
    const swal = Swal.mixin({
        customClass: {
            title: 'sweet-title',
        }
    });
    swal.fire({
      icon: 'success',
      title: @json(session('success')),
      showConfirmButton: false,
      timer: 1500
    })
</script>
@endif
