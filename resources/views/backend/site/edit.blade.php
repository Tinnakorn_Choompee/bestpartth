<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('messages.edit', ['name' => trans('messages.site')]) }}
        </h2>
    </x-slot>

    <x-slot name="back"> {{ route('dashboard') }} </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    <form method="POST" action="{{ route('site.update', $site->id) }}">
                        @method('PATCH')
                        @csrf
                        <div class="mt-4 mb-6">
                            <x-label for="name" :value="__('Name')" />
                            <x-input id="name" class="block mt-1 w-full" type="text" name="name" value="{{ $site->name }}" disabled />
                        </div>
                        <div class="mt-4 mb-6">
                            <x-label for="title" :value="__('Title')" />
                            <x-input id="title" class="block mt-1 w-full" type="text" name="title" value="{{ $site->title }}" required autofocus />
                        </div>
                        <div class="mt-4 mb-6">
                            <x-label for="description" :value="__('Description')" />
                            <textarea name="description" cols="30" rows="10" class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">{{ $site->description }}</textarea>
                        </div>
                        <div class="mt-4 mb-6">
                            <x-label for="keywords" :value="__('Keywords')" />
                            <textarea name="keywords" cols="30" rows="10" class="block mt-1 w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">{{ $site->keywords }}</textarea>
                        </div>
                        <div class="flex items-center justify-center mt-4 mb-6">
                            <x-button class="w-full my-5 h-12 text-base">
                                {{ __('form.save') }}
                            </x-button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>


