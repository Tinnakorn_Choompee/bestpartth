<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('messages.create', ['name' => trans('messages.tag')]) }}
        </h2>
    </x-slot>

    <x-slot name="back"> {{ route('tags.index') }} </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    <form method="POST" action="{{ route('tags.store') }}">
                        @csrf
                        <div class="mt-4 mb-6">
                            <x-label for="name" :value="__('form.name')" />
                            <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus />
                        </div>
                        <div class="flex items-center justify-center mt-4 mb-6">
                            <x-button class="w-full my-5 h-12 text-base">
                                {{ __('form.save') }}
                            </x-button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>


