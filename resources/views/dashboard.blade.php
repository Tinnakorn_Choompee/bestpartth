<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('menu.dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6">
                    <main class="">
                        <div class="grid mb-4 lg:p-8 sm:px-6 mx-4 rounded-3xl border-4">
                            <div class="grid grid-cols-12 gap-6">
                                <div class="grid grid-cols-12 col-span-12 gap-6 xxl:col-span-9">
                                    <div class="col-span-12 mt-2">
                                        <div class="grid grid-cols-12 gap-6 mt-5">
                                            <a class="border-2 transform hover:scale-105 transition duration-300 shadow-md rounded-lg col-span-12 sm:col-span-6 xl:col-span-3 intro-y bg-white"
                                                href="{{ route('users.index') }}">
                                                <div class="p-6">
                                                    <div class="flex justify-between">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                            class="h-7 w-7 text-blue-400" fill="none"
                                                            viewBox="0 0 24 24" stroke="currentColor">
                                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                                stroke-width="2"
                                                                d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
                                                        </svg>
                                                        <div
                                                            class="bg-blue-500 rounded-full h-6 px-2 flex justify-items-center text-white font-semibold text-sm">
                                                            <span class="flex items-center"> {{ __('messages.user') }} </span>
                                                        </div>
                                                    </div>
                                                    <div class="ml-2 w-full flex-1">
                                                        <div>
                                                            <div class="mt-3 text-3xl font-bold leading-8">{{ $user }}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                            <a class="border-2 transform hover:scale-105 transition duration-300 shadow-md rounded-lg col-span-12 sm:col-span-6 xl:col-span-3 intro-y bg-white"
                                                href="{{ route('categories.index') }}">
                                                <div class="p-5">
                                                    <div class="flex justify-between">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                            class="h-7 w-7 text-yellow-400" fill="none"
                                                            viewBox="0 0 24 24" stroke="currentColor">
                                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                                stroke-width="2"
                                                                d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01" />
                                                        </svg>
                                                        <div
                                                            class="bg-yellow-500 rounded-full h-6 px-2 flex justify-items-center text-white font-semibold text-sm">
                                                            <span class="flex items-center"> {{ __('messages.category') }} </span>
                                                        </div>
                                                    </div>
                                                    <div class="ml-2 w-full flex-1">
                                                        <div>
                                                            <div class="mt-3 text-3xl font-bold leading-8">{{ $category }}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                            <a class="border-2 transform hover:scale-105 transition duration-300 shadow-md rounded-lg col-span-12 sm:col-span-6 xl:col-span-3 intro-y bg-white"
                                                href="{{ route('tags.index') }}">
                                                <div class="p-5">
                                                    <div class="flex justify-between">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                            class="h-7 w-7 text-pink-600" fill="none"
                                                            viewBox="0 0 24 24" stroke="currentColor">
                                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                                stroke-width="2"
                                                                d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                                                        </svg>
                                                        <div
                                                            class="bg-pink-500 rounded-full h-6 px-2 flex justify-items-center text-white font-semibold text-sm">
                                                            <span class="flex items-center"> {{ __('messages.tag') }} </span>
                                                        </div>
                                                    </div>
                                                    <div class="ml-2 w-full flex-1">
                                                        <div>
                                                            <div class="mt-3 text-3xl font-bold leading-8">{{ $tag }}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                            <a class="border-2 transform hover:scale-105 transition duration-300 shadow-md rounded-lg col-span-12 sm:col-span-6 xl:col-span-3 intro-y bg-white"
                                                href="{{ route('products.index') }}">
                                                <div class="p-5">
                                                    <div class="flex justify-between">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                            class="h-7 w-7 text-green-400" fill="none"
                                                            viewBox="0 0 24 24" stroke="currentColor">
                                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                                stroke-width="2"
                                                                d="M16 11V7a4 4 0 00-8 0v4M5 9h14l1 12H4L5 9z" />
                                                        </svg>
                                                        <div
                                                            class="bg-green-500 rounded-full h-6 px-2 flex justify-items-center text-white font-semibold text-sm">
                                                            <span class="flex items-center"> {{ __('messages.product') }} </span>
                                                        </div>
                                                    </div>
                                                    <div class="ml-2 w-full flex-1">
                                                        <div>
                                                            <div class="mt-3 text-3xl font-bold leading-8">{{ $product }}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>

                                    @role('admin')
                                    <div class="col-span-12 mt-5">
                                        <div class="grid gap-2 grid-cols-1 lg:grid-cols-1">
                                            <div class="bg-white p-4 shadow-lg rounded-lg">
                                                <h1 class="font-bold text-base"> {{ __('table.setting_website') }} </h1>
                                                <div class="mt-4">
                                                    <div class="flex flex-col">
                                                        <div class="-my-2 overflow-x-auto">
                                                            <div class="py-2 align-middle inline-block min-w-full">
                                                                <div
                                                                    class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg bg-white flex items-center">
                                                                    <table class="min-w-full divide-y divide-gray-200">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="px-6 py-3 bg-gray-50 text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                                                                    <div class="flex item-left justify-start cursor-pointer">
                                                                                        <span class="mr-2">{{ __('table.website') }}</span>
                                                                                    </div>
                                                                                </th>
                                                                                <th class="px-6 py-3 bg-gray-50 text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                                                                    <div class="flex item-left justify-start cursor-pointer">
                                                                                        <span class="mr-2">{{ __('table.title') }}</span>
                                                                                    </div>
                                                                                </th>
                                                                                <th class="px-6 py-3 bg-gray-50 text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                                                                    <div class="flex item-left justify-start cursor-pointer">
                                                                                        <span class="mr-2">{{ __('table.description') }}</span>
                                                                                    </div>
                                                                                </th>
                                                                                <th class="px-6 py-3 bg-gray-50 text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                                                                    <div class="flex item-left justify-start cursor-pointer">
                                                                                        <span class="mr-2">{{ __('table.keywords') }}</span>
                                                                                    </div>
                                                                                </th>
                                                                                <th class="px-6 py-3 bg-gray-50 text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                                                                    <div class=" cursor-pointer">
                                                                                        <span class="mr-2">{{ __('Action') }}</span>
                                                                                    </div>
                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody class="bg-white divide-y divide-gray-200 items-center">
                                                                            @foreach ($sites as $site)
                                                                            <tr>
                                                                                <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5">
                                                                                    <p> {{ $site->name }} </p>
                                                                                </td>
                                                                                <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5">
                                                                                    <p> {{ $site->title }} </p>
                                                                                </td>
                                                                                <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5">
                                                                                    <p> {{ $site->description }} </p>
                                                                                </td>
                                                                                <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5">
                                                                                    <p> {{ $site->keywords }} </p>
                                                                                </td>
                                                                                <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5">
                                                                                    <div class="flex item-center justify-center">
                                                                                    <a href="{{ route('site.edit', $site->name) }}">
                                                                                        <div class="w-5 mr-2 transform text-yellow-500 hover:text-yellow-500 hover:scale-110">
                                                                                            <svg fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z"  />
                                                                                            </svg>
                                                                                        </div>
                                                                                    </a>
                                                                                    </div>
                                                                                </td>
                                                                            </tr> 
                                                                            @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endrole

                                    <div class="col-span-12 mt-5">
                                        <div class="grid gap-2 grid-cols-1 lg:grid-cols-1">
                                            <div class="bg-white p-4 shadow-lg rounded-lg">
                                                <h1 class="font-bold text-base"> {{ __('table.latest_product') }} </h1>
                                                <div class="mt-4">
                                                    <div class="flex flex-col">
                                                        <div class="-my-2 overflow-x-auto">
                                                            <div class="py-2 align-middle inline-block min-w-full">
                                                                <div
                                                                    class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg bg-white flex items-center">
                                                                    <table class="min-w-full divide-y divide-gray-200">
                                                                        <thead>
                                                                            <tr>
                                                                                <th
                                                                                    class="px-6 py-3 bg-gray-50 text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                                                                    <div class=" cursor-pointer">
                                                                                        <span class="mr-2">{{ __('table.name') }}</span>
                                                                                    </div>
                                                                                </th>
                                                                                <th
                                                                                    class="px-6 py-3 bg-gray-50 text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                                                                    <div class=" cursor-pointer">
                                                                                        <span class="mr-2">{{ __('table.created_at') }}</span>
                                                                                    </div>
                                                                                </th>
                                                                                <th
                                                                                    class="px-6 py-3 bg-gray-50 text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                                                                    <div class=" cursor-pointer">
                                                                                        <span class="mr-2">{{ __('table.actions') }}</span>
                                                                                    </div>
                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody
                                                                            class="bg-white divide-y divide-gray-200 items-center">
                                                                            @forelse ($latest_product as $product)
                                                                            <tr>
                                                                                <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5">
                                                                                    <p> {{ $product->name }}</p>
                                                                                </td>
                                                                                <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5 text-center">
                                                                                    <p>{{ date_format($product->created_at, 'd/m/Y | H:i:s') }}</p>
                                                                                </td>
                                                                                <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5">
                                                                                    @include('includes.action', ['name' => 'products', 'id' => $product->id])
                                                                                </td>
                                                                            </tr>
                                                                            @empty
                                                                            <tr>
                                                                                <td class="px-6 py-4 whitespace-no-wrap text-sm leading-5" colspan="4"> No Product Data.. </td>
                                                                            </tr>
                                                                            @endforelse
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>