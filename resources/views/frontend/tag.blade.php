@extends('frontend.layouts.app')

@section('title', $site->title ?? 'แท็กสินค้า | BESTPARTTH')

@section('description', $site->description ?? 'แนะนำสินค้าออนไลน์')

@section('keywords', $site->keywords ?? 'แนะนำสินค้าออนไลน์')

@section('content')
<!-- start main content -->
<main class="main-container">
	<section id="popular-product" class="ecommerce">
		<div class="container">
			<!-- Shopping items content -->
			<div class="shopping-content">
				<!-- Block heading two -->
				<div class="block-heading-two">
					<h3><span> สินค้าแท็ก {{ $tag_name }}</span></h3>
				</div>
				@include('frontend.layouts.product')
			</div>
		</div>
	</section>
	<!-- Start Our Shop Items -->
	@include('frontend.layouts.recommended')
</main>
<!-- end main content -->
@endsection

@push('scripts')
<script type="text/javascript">
	if (jQuery().owlCarousel) {
		var productCarousel = $("#product-carousel");
		productCarousel.owlCarousel({
			loop: true,
			dots: false,
			responsive: {
				0: {
					items: 1
				},
				480: {
					items: 2
				},
				900: {
					items: 3
				},
				1100: {
					items: 4
				}
			}
		});

		// Custom Navigation Events
		$(".product-control-nav .next").on("click", function() {
			productCarousel.trigger('next.owl.carousel');
		});

		$(".product-control-nav .prev").on("click", function() {
			productCarousel.trigger('prev.owl.carousel');
		});
	}
</script>
@endpush