<!-- Recent items Starts -->
<section id="recent-product">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <section class="related-products">
                    <!-- Block heading two -->
                    <div class="block-heading-two">
                        <h3><span>สินค้าแนะนำ</span></h3>
                    </div>
                    <div class="related-products-wrapper">
                        <div class="related-products-carousel">
                            <div class="product-control-nav">
                                <a class="prev"><i class="fa fa-angle-left"></i></a>
                                <a class="next"><i class="fa fa-angle-right"></i></a>
                            </div>
                            <div class="row product-listing">
                                <div id="product-carousel" class="product-listing">
                                    @forelse ($Recommended as $product)
                                    <div class="product  item first ">
                                        <article>
                                            <figure>
                                                <a href="{{ route('product.detail', $product->slug) }}">
                                                    <img src="{{ asset('images/product/'.$product->image) }}"
                                                        class="img-responsive" alt="T_2_front">
                                                </a>
                                                <figcaption>
                                                    <span class="amount"> ฿ {{ number_format($product->amount) }}
                                                    </span>
                                                </figcaption>
                                            </figure>
                                            <h4 class="title"><a
                                                    href="{{ route('product.detail', $product->slug) }}">{{ $product->name }}</a>
                                            </h4>
                                            <a href="{{ route('product.detail', $product->slug) }}"
                                                class="button-add-to-cart"> รายละเอียด </a>
                                        </article>
                                    </div>
                                    @empty
                                    <p class="text-left"> ไม่มีสินค้าแนะนำ </p>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</section>
<!-- Recent items Ends -->