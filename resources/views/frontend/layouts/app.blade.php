<!DOCTYPE html>
<html class="no-js">

<head>
	<title> @yield('title') </title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="@yield('description')">
	<meta name="keywords" content="@yield('keywords')">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Fav icon -->
	<link rel="icon" type="image/ico" href="{{ asset('logo.ico') }}" />

	<!-- Font -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,900,700,700italic,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700%7CDancing+Script%7CMontserrat:400,700%7CMerriweather:400,300italic%7CLato:400,700,900' rel='stylesheet' type='text/css' />
	<link href='http://fonts.googleapis.com/css?family=Cantata+One' rel='stylesheet' type='text/css' />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700,600' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,300,500,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="{{ asset('templates/css/bootstrap.min.css') }}">

	<link rel="stylesheet" href="{{ asset('templates/css/flexslider.css') }}" />
	<link rel="stylesheet" href="{{ asset('templates/css/image-light-box.css') }}"/>

	<!-- Magnific Popup -->
	<link href="{{ asset('templates/css/magnific-popup.css') }}" rel="stylesheet">

	<link rel="stylesheet" href="{{ asset('templates/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('templates/css/normalize.css') }}">
	<link rel="stylesheet" href="{{ asset('templates/css/skin-lblue.css') }}">

	<link rel="stylesheet" href="{{ asset('templates/css/ecommerce.css') }}">

	<!-- Owl carousel -->
	<link href="{{ asset('templates/css/owl.carousel.css') }}" rel="stylesheet">

	<link rel="stylesheet" href="{{ asset('templates/css/main.css') }}">
	<link rel="stylesheet" href="{{ asset('templates/style.css') }}">
	<link rel="stylesheet" href="{{ asset('templates/css/setting.css') }}">
	<link rel="stylesheet" href="{{ asset('templates/css/responsive.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('templates/css/revolutionslider_settings.css') }}" media="screen" />

	<script src="{{ asset('templates/js/vendor/modernizr-2.6.2.min.js') }}"></script>
</head>

<body class="style-14">
    @include('frontend.layouts.loading')
    @include('frontend.layouts.header')

    @yield('content')

    @include('frontend.layouts.footer')

	<!-- All script -->
	<script src="{{ asset('templates/js/vendor/jquery-1.10.2.min.js') }}"></script>
	<script src="{{ asset('templates/js/bootstrap.min.js') }}"></script>

	<!-- Scroll up js ============================================ -->
	<script src="{{ asset('templates/js/jquery.scrollUp.js') }}"></script>
	<script src="{{ asset('templates/js/menu.js/') }}"></script>

	<!-- new collection section script -->
	<script src="{{ asset('templates/js/swiper/idangerous.swiper.min.js') }}"></script>
	<script src="{{ asset('templates/js/swiper/swiper.custom.js') }}"></script>

	<!-- Magnific Popup -->
	<script src="{{ asset('templates/js/jquery.magnific-popup.min.js') }}"></script>
	<script src="{{ asset('templates/js/jquery.countdown.min.js') }}"></script>

	<!-- SLIDER REVOLUTION SCRIPTS  -->
	<script type="text/javascript" src="{{ asset('templates/rs-plugin/js/jquery.themepunch.plugins.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('templates/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
	<!-- Owl carousel -->
	<script src="{{ asset('templates/js/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('templates/js/main.js') }}"></script>

	@stack('scripts')
</body>
</html>