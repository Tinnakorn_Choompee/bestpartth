<div class="related-products-wrapper">
    <div class="related-products-carousel">
        <div class="products-top"></div>
        <h3 class="section-title"> สินค้าที่เกี่ยวข้อง </h3>
        <div class="row product-listing">
            <div id="product-carousel" class="product-listing">
                @forelse ($related as $product)
                <div class="product item first ">
                    <article>
                        <figure>
                            <a href="{{ route('product.detail', $product->slug) }}">
                                <img src="{{ asset('images/product/'. $product->image) }}" class="img-responsive" alt="T_2_front">
                            </a>
                            <figcaption><span class="amount">฿ {{ number_format($product->amount) }}</span></figcaption>
                        </figure>
                        <h4 class="title"><a href="{{ route('product.detail', $product->slug) }}"> {{ $product->name }} </a></h4>
                        <a href="{{ route('product.detail', $product->slug) }}" class="button-add-to-cart"> รายละเอียด </a>
                    </article>
                </div>
                @empty
                <p class="text-left" style="font-size:20px; margin:30px 30px"> ไม่มีสินค้า </p>
                @endforelse
            </div>
        </div>
    </div>
</div>