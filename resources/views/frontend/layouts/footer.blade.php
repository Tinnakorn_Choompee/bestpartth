<!-- start footer area -->
<footer class="footer-area-content">
    <div class="footer-bottom footer-wrapper style-3">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer-bottom-navigation">
                        <div class="cell-view text-center">
                            <div class="copyright">Created with by <a href="#"> BESTPARTTH </a> . All right reserved
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer area end -->