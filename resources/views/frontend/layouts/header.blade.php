<!-- start header -->
<header>
    <!-- Top bar starts -->
    <div class="top-bar">
        <div class="container">
            <!-- Search section for responsive design -->
            <div class="tb-search pull-left">
                <a href="#" class="b-dropdown"><i class="fa fa-search square-2 rounded-1 bg-color white"></i></a>
                <div class="b-dropdown-block">
                    <form action>
                        <!-- Input Group -->
                        <div class="input-group">
                            <input type="text" name="search" class="form-control" placeholder="search">
                            <span class="input-group-btn">
                                <button class="btn btn-color" type="submit">ค้นหาสินค้า</button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <!-- Search section ends -->
            <!-- Social media starts -->
            <div class="tb-social pull-right">
                <div class="brand-bg text-right">
                    <!-- Brand Icons -->
                    <a href="#" class="facebook"><i class="fa fa-facebook square-2 rounded-1"></i></a>
                    <a href="#" class="twitter"><i class="fa fa-twitter square-2 rounded-1"></i></a>
                    <a href="#" class="google-plus"><i class="fa fa-google-plus square-2 rounded-1"></i></a>
                </div>
            </div>
            <!-- Social media ends -->
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- Top bar ends -->
    <!-- Header One Starts -->
    <div class="header-1">
        <!-- Container -->
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <!-- Logo section -->
                    <div class="logo">
                        <h1><a href="{{ route('index') }}"><i class="fa fa-shopping-cart"></i> BESTPARTTH </a></h1>
                    </div>
                </div>
                <div class="col-md-6 col-md-offset-2 col-sm-5 col-sm-offset-3 hidden-xs">
                    <!-- Search Form -->
                    <div class="header-search">
                        <form>
                            <!-- Input Group -->
                            <div class="input-group">
                                <input type="text" name="search" class="form-control" placeholder="search">
                                <span class="input-group-btn">
                                    <button class="btn btn-color" type="submit">ค้นหาสินค้า</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Navigation starts -->
        <div class="navi">
            <div class="container">
                <div class="navy">
                    <ul>
                        @foreach ($categories as $p)
                            @if ($p->child->count() && $p->parent_id == 0)
                                <li><a href="{{ route('product.category', $p->name) }}">{{ $p->name }}</a>
                                    <ul>
                                    @foreach ($p->child as $item)
                                        <li><a href="{{ route('product.category', $item->name) }}"> {{ $item->name }}</a>
                                            @includeWhen($item->child->count(), 'includes.submenu', ['item' => $item, 'count' => $item->child->count()])
                                        </li>
                                    @endforeach
                                    </ul>
                                </li>
                            @endif
                            @if($p->parent_id == 0 && $p->child->count() == 0)
                                <li><a href="{{ $p->name }}">{{ $p->name }}</a></li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <!-- Navigation ends -->
    </div>
    <!-- Header one ends -->
</header>
<!-- end header -->