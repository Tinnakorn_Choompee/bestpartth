<div class="row">
    @forelse ($products as $product)
    <div class="col-md-3 col-sm-6">
        <!-- Shopping items -->
        <div class="shopping-item">
            <!-- Image -->
            <a href="{{ route('product.detail', $product->slug) }}">
                <img class="img-responsive" src="{{ asset('images/product/'. $product->image) }}" alt="" /></a>
            <!-- Shopping item name / Heading -->
                <h4><a href="{{ route('product.detail', $product->slug) }}">{{ $product->name }}</a><span
                    class="color pull-right"> ฿ {{ number_format($product->amount) }}</span></h4>
            <div class="clearfix"></div>
            <!-- Buy now button -->
            <div class="visible-xs">
                <a class="btn btn-color btn-sm btn-block" href="{{ route('product.detail', $product->slug) }}">รายละเอียด</a>
            </div>
            <div class="item-hover bg-color hidden-xs">
                <a href="{{ route('product.detail', $product->slug) }}"> รายละเอียด </a>
            </div>
            @if ((date("Y-m-d H:i:s", strtotime($product->created_at)) > date("Y-m-d H:i:s", strtotime('-2 hours'))) && $product->hot)
                <span class="hot-tag bg-red">HOT</span>
                <span class="hot-tag bg-lblue" style="margin-right: 35px;">NEW</span>
            @elseif ($product->hot)
                <span class="hot-tag bg-red">HOT</span>
            @elseif (date("Y-m-d H:i:s", strtotime($product->created_at)) > date("Y-m-d H:i:s", strtotime('-2 hours')))
                <span class="hot-tag bg-lblue">NEW</span>
            @endif
        </div>
    </div>
    @empty
    <p class="text-left" style="font-size:20px; margin:30px 30px"> ไม่มีสินค้า </p>
    @endforelse
</div>
<!-- Pagination -->
<div class="shopping-pagination pull-right">
    {{ $products->links('vendor.pagination.default') }}
</div>
<!-- Pagination end-->