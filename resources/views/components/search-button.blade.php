<button {{ $attributes->merge(['type' => 'submit', 'class' => 'block w-7 h-7 text-center text-xl leading-0 absolute top-2 right-2 text-gray-400 focus:outline-none hover:text-gray-900 transition-colors']) }}>
    {{ $slot }}
</button>