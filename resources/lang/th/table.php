<?php

return [
    'search' => 'ค้นหา...',
    'no_data' => 'ไม่มีข้อมูล...',
    'name' => 'ชื่อ',
    'username' => 'ชื่อผู้ใช้',
    'status' => 'สถานะ',
    'actions' => 'กระทำ',
    'parent'=> 'ประเภทหลัก',
    'child'=> 'ประเภทย่อย',
    'no_child'=> 'ไม่มีประเภทย่อย',
    'no_child'=> 'ไม่มีประเภทย่อย',
    'latest_product'=> 'รายการสินค้าล่าสด',
    'amount'=> 'ราคา',
    'created_at'=> 'วัน/เวลาที่สร้าง',
    'setting_website'=> 'การตั้งค่าเว็บไซต์',
    'website'=> 'Site',
    'title'=> 'Title',
    'description'=> 'Description',
    'keywords'=> 'Keywords',
];
