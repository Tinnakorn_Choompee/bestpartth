<?php

return [
    'dashboard' => 'หน้าหลัก',
    'user' => 'ผู้ใช้งาน',
    'category' => 'ประเภทสินค้า',
    'tag' => 'ประเภทแท็ก',
    'product' => 'สินค้า',
];