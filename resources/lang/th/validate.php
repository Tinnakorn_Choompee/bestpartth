<?php

return [
    'roles.required' => 'กรุณาเลือกประเภทผู้ใช้',
    'name.required' => 'กรุณากรอกชื่อ',
    'name.unique' => 'มีชื่อนี้ในระบบแล้ว',
    'username.required' => 'กรุณากรอกชื่อผู้ใช้',
    'username.unique' => 'มีชื่อผู้ใช้นี้ในระบบแล้ว',
    'password.required' => 'กรุณากรอกรหัสผ่าน',
    'password.min' => 'กรุณากรอกรหัสผ่าน :min ตัวขึ้นไป',
    'password.confirmed' => 'รหัสผ่านยืนยันไม่ถูกต้อง',
    'parent_id.required' => 'กรุณาเลือกประเภทย่อย',
    'detail.required' => 'กรุณากรอกรายละเอียด',
    'description.required' => 'กรุณากรอกคำอธิบาย',
    'image.required' => 'กรุณาเลือกรูปภาพ',
    'image.mimes' => 'กรุณาเลือกรูปภาพนามสกุล jpeg,png,jpg',
    'category.required' => 'กรุณาเลือกประเภทสินค้า',
    'tags.required' => 'กรุณาเลือกแท็กสินค้า',
];