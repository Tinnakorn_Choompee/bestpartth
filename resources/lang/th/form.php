<?php

return [
    'save' => 'บันทึก',
    'name' => 'ชื่อ',
    'username' => 'ชื่อผู้ใช้',
    'password' => 'รหัสผ่าน',
    'password_confirmation' => 'ยืนยันรหัสผ่าน',
    'roles' => 'ประเภทผู้ใช้',
    'status' => 'สถานะ',
    'status_detail' => [
        'active' => 'เปิดใช้งาน',
        'in_active' => 'ปิดใช้งาน'
    ],
    'hot' => 'สินค้ายอดนิยม',
    'recommended' => 'สินค้าแนะนำ',
    'detail' => 'รายละเอียด',
    'description' => 'คำอธิบาย',
    'parent'=> 'ประเภทย่อย',
    'select_parent'=> 'เลือกประเภทย่อย',
    'no_parent'=> 'ไม่มีประเภทย่อย',
    'confirm_delete' => 'ยืนยันที่จะลบข้อมูลนี้ ?',
    'delete' => 'ลบ',
    'cancel' => 'ยกเลิก',
    'price' => 'ราคาต้นทุน',
    'amount' => 'ราคาขาย',
    'category' => 'ประเภทสินค้า',
    'link' => 'ลิ้งค์เชื่อมโยง',
    'image' => 'รูปสินค้าหลัก',
    'gallery' => 'รูปสินค้าย่อย',
    'select_file' => 'เลือกไฟล์',
    'drag_drop' => 'ลากและวางไฟล์ที่นี้',
    'upload_file' => 'อัปโหลดไฟล์',
    'upload_list' => 'รายการอัปโหลด',
    'no_files_selected' => 'ไม่ได้เลือกไฟล์',
    'tags' => 'แท็กสินค้า',
    'select_tags' => '-- เลือกแท็กสินค้า --',
    'select_category' => '-- เลือกประเภทสินค้า --'
];