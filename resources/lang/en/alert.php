<?php

return [
    'created' => 'CREATED',
    'updated' => 'UPDATED',
    'deleted' => 'DELETED',
];