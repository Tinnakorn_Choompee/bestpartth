<?php

return [
    'detail' => 'Detail',
    'price' => 'Price',
    'amount' => 'Amount',
    'category' => 'Category',
    'link' => 'Link',
    'tags' => 'Tags',
    'description' => 'Description',
    'status' => 'Status',
    'hot' => 'Hot Product',
    'recommended' => 'Recommended Product',
];

