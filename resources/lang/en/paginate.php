<?php

return [
    'show' => 'Showing',
    'to' => 'to',
    'of' => 'of',
    'results' => 'results',
];
