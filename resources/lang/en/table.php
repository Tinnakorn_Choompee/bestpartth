<?php

return [
    'search' => 'Search...',
    'no_data' => 'No data...',
    'name' => 'Name',
    'username' => 'Username',
    'status' => 'Status',
    'actions' => 'Actions',
    'parent'=> 'Parant',
    'child'=> 'Child',
    'latest_product'=> 'Latest Product',
    'amount'=> 'Amount',
    'created_at'=> 'Created At',
    'setting_website'=> 'Setting Website',
    'website'=> 'Site',
    'title'=> 'Title',
    'description'=> 'Description',
    'keywords'=> 'Keywords',
];