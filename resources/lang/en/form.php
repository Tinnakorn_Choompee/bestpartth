<?php

return [
    'save' => 'Save',
    'name' => 'Name',
    'username' => 'Username',
    'password' => 'Password',
    'password_confirmation' => 'Confirm Password',
    'roles' => 'Roles',
    'status' => 'Staus',
    'status_detail' => [
        'active' => 'Active',
        'in_active' => 'In Active'
    ],
    'hot' => 'Hot Product',
    'recommended' => 'Recommended Product',
    'detail' => 'Detail',
    'description' => 'Description',
    'parent'=> 'Parent',
    'select_parent'=> 'Select Parent',
    'no_parent'=> 'No Parent',
    'confirm_delete' => 'Do you want to delete ?',
    'delete' => 'Delete',
    'cancel' => 'Cancel',
    'price' => 'Price',
    'amount' => 'Amount',
    'category' => 'Category',
    'link' => 'Link',
    'image' => 'Image',
    'gallery' => 'Gallery',
    'select_file' => 'Select a file',
    'drag_drop' => 'Drag and drop your files anywhere or',
    'upload_file' => 'Upload a file',
    'upload_list' => 'To Upload',
    'no_files_selected' => 'No files selected',
    'tags' => 'Tags',
    'select_tags' => 'Selete tags',
    'select_category' => 'Selete Category'
];