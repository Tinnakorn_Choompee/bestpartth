<?php

return [
    'error' => 'Whoops! Something went wrong.',
    'add' => 'Add',
    'back' => 'Back',
    'logout' => 'Log out',

    'user' => 'User',
    'category' => 'Category',
    'tag' => 'Tag',
    'product' => 'Product',

    'create' => 'Create :name',
    'edit' => 'Edit :name',
    'show' => 'Show :name'
];