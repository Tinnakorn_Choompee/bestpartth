<?php

return [
    'roles.required' => 'The roles is required.',
    'name.required' => 'The name is required.',
    'name.unique' => 'The name has already been taken.',
    'username.required' => 'The username is required.',
    'username.unique' => 'The username has already been taken.',
    'password.required' => 'The password is required.',
    'password.min' => 'The password must be at least :min characters.',
    'password.confirmed' => 'The password confirmation does not match.',
    'parent_id.required' => 'The parent is required.',
    'detail.required' => 'The detail is required.',
    'description.required' => 'The description is required.',
    'image.required' => 'The image is required.',
    'image.mimes' => 'The image must be a file of type jpeg,png,jpg',
    'category.required' => 'The category is required.',
    'tags.required' => 'The tags is required.',
];