<?php

return [
    'dashboard' => 'Dashboard',
    'user' => 'User',
    'category' => 'Category',
    'tag' => 'Tag',
    'product' => 'Product',
];